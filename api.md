# API Rest de Anótame.la
anotamela es una applicacion que me permite crear notas de codigo para mis clases

## Metodos HTTP permitidos

|  Método  | Descriptión                            |
| -------- | -------------------------------------- |
| 'GET'    | Obtener un recurso o lista de recursos |
| 'POST'   | Crear un recurso                       |
| 'PUT'    | Actualizar un recurso                  |
| 'DELETE' | Eliminar un recurso                    |

# Crear una nota nueva [POST]
Solicitud

	{
		nota: {
			"title": "Mejorando.la #node-pro",
			"description": "Introduccion a la clase",
			"type": "js",
			"body": "Look mom! I know JS"
		}
	}

Respuesta

	{
		nota: {
			"id": 123,
			"title": "Hello",
			"description": "Introduccion",
			"type": "js",
			"body": "Look mom! I know JS"
		}
	}

## Obtener una nota
Solicitud GET /notas/123

Respuesta
	{
		nota: {
			"id": 123,
			"title": "Hello",
			"description": "Introduccion",
			"type": "js",
			"body": "Look mom! I know JS"
		}
	}

## Actualizar una nota
Solicitud PUT /notas/123

	{
		nota: {
			"id": 123,
			"title",
			"description": "Introduccion",
			"type": "ruby",
			"body": "Look mom! I know Ruby"
		}
	}

## Actualizar una nota
Solicitud PATCH /notas/123

	{
		nota: {
			"id": 123,
			"title": "Hello",
			"description": "Introduccion",
			"type": "js",
			"body": "Look mom! I know JS"
		}
	}

Respuesta

	{
		nota: {
			"id": 123,
			"title": "Hello",
			"description": "Introduccion",
			"type": "js",
			"body": "Look mom! I know JS"
		}
	}

## Eliminar una nota
Solicitud DELETE /nota/id

	{
		nota: {
			"id": 123,
			"title": "Hello",
			"description": "Introduccion",
			"type": "js",
			"body": "Look mom! I know JS"
		}
	}