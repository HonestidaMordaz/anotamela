/**
 * Dependencies
 */
var express = require("express");
var bodyParser = require('body-parser');

/*
 * Local Variables
 */
var app = module.exports = express();

/*
 * Middleware
 */
server.use(bodyParser.json('application/json'));

/**
 * Routes
 */
var notas = require('./lib/notas');
server.use(notas);

/*
 * Expose or start server
 */
if(!module.parent) {
	server.listen(300, function() {
		console.log('Server running at port 3000');
	});
} else {
	module.exports = server;
}