var app = require('express')();
var db = {};

/**
 * Routes
 */
app.route('/notas/:id?')
	.all(function (req, res, next) {
		console.log(req.method, req.path, req.body);
		res.set('Content-Type', 'application/json');
		next();
	})

	.post(function(req, res) {
		console.log('POST', req.body);
		
		var notaNueva = req.body.nota;
		notaNueva.id = Date.now();

		db[notaNueva.id] = notaNueva;

		res
			.status(201)
			.json({
				nota: notaNueva
			});
	})

	.get(function (req, res) {
		console.log('GET /notas/%s', req.params.id);

		var id = req.params.id;
		var nota = db[id];

		res.json({
			notas : nota
		});
	})

	.put(function (req, res) {
		var id = req.params.id;
		var notaActualizada = req.body.nota;

		db[id] = notaActualizada;

		res
			.json({
				nota: [db[id]]
			});
	});

module.exports = app;