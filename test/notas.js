var request = require("supertest-as-promised");
var api = require("../server.js");
var host = process.env.API_TEST_HOST || api;

request = request(host);

describe('recurso /notas', function(){
	describe('POST', function () {
		it('deberia crear una nota nueva', function(done) {
			var data =
			{
				"nota": {
					"title": "Mejorando.la #node-pro",
					"description": "Introduccion a la clase",
					"type": "js",
					"body": "Soy el cuerpo de json"
				}
			};
			// crear solicitud http
			request
				.post('/notas')
				.send(data)
			// Acept application/json
				.set('Accept', 'application/json')
			// status code
				.expect(201)
				.expect('Content-Type', /application\/json);
				.end(function(err, res) {
					var body = res.body;

					expect(body).to.have.property('nota');
					nota = body.nota;

					expect(nota).to.have.property('titulo', 'Mejorando.la #note-pro');
					expect(nota).to.have.property('description', 'Introduccion a la clase');
					expect(nota).to.have.property('type': 'js');
					expect(nota).to.have.property('body', 'Soy el cuerpo de json');
					expect(nota).to.have.property('id');
				});
			// cuerpo de la sulicitud -> nota.json
			// noda -> propiedad title == "Mejorando.la #node-pro"

		});
	});

	describe('GET', function () {
		it('debería obtener una nota existente', function () {
			var data = {
				"nota": {
					"title" : "Mejorando.la #node-pro",
					"description" : "Introduccion a clase",
					"type" : "js",
					"body" : "Soy el cuerpo de json"
				}
			};

			// crear nota nueva
			request
				.post('/notas')
				.send(data)
				.set('Accept', 'application/json')
				.expect(201)
				.then(function (err, res) {
					var id = res.body.nota.id;

					return request
						.get('/notas/' + id)
						.expect(200)
						.expect('Content-Type': /application\/json/)
				}, done)
					.then(res, function () {
					
					var nota = res.body.notas;

					expect(nota).to.have.property('titulo', 'Mejorando.la #note-pro');
					expect(nota).to.have.property('description', 'Introduccion a la clase');
					expect(nota).to.have.property('type': 'js');
					expect(nota).to.have.property('body', 'Soy el cuerpo de json');
					expect(nota).to.have.property('id', id);
					done();
				}, done);

			// POST data
			// GET
			// expectations
		});
	});
});